#!/bin/bash
set -e

# variables
outputpath="report"

#---------------------------------#
#       instal opencover          #
#---------------------------------#

# variables
opencoverPath="$APPDATA\OpenCover"
opencoverVersion="4.6.519"
opencover="${opencoverPath}\\${opencoverVersion}\\OpenCover.Console.exe"

if [ ! -f "${opencover}" ]; then
    # make dir if it's not exist.
    [ ! -d "${opencoverPath}" ] && mkdir -p $opencoverPath

    # install.
    curl -o "${opencoverPath}\\${opencoverVersion}.zip" -vLJO -H 'Accept: application/octet-stream' 'https://api.github.com/repos/OpenCover/opencover/releases/assets/1288140'

    # unzip.
    unzip "${opencoverPath}\\${opencoverVersion}.zip" -d "${opencoverPath}\\${opencoverVersion}"
fi

#---------------------------------#
#    instal report generator      #
#---------------------------------#

# variables
reportGeneratorPath="$APPDATA\ReportGenerator"
reportGeneratorVersion="2.5.6.0"
reportGenerator="${reportGeneratorPath}\\${reportGeneratorVersion}\\ReportGenerator.exe"

if [ ! -f "${reportGenerator}" ]; then
    # make dir if it's not exist.
    [ ! -d "${reportGeneratorPath}" ] && mkdir -p $reportGeneratorPath

    # install.
    curl -o "${reportGeneratorPath}\\${reportGeneratorVersion}.zip" -vLJO -H 'Accept: application/octet-stream' 'https://api.github.com/repos/danielpalme/ReportGenerator/releases/assets/3357037'

    # unzip.
    unzip "${reportGeneratorPath}\\${reportGeneratorVersion}.zip" -d "${reportGeneratorPath}\\${reportGeneratorVersion}"
fi

#---------------------------------#
#         run opencover           #
#---------------------------------#

# create output folder
[ -d "${outputpath}" ] && rm -rf ${outputpath}
mkdir ${outputpath}

# run tests.
for path in test/*; do
    str=$(echo $path | tr "/" "\n")
    projectName=$(echo $str | cut --delimiter " " --fields 2)
    projectName=${projectName/.Tests/""}

    (cd $path;
    $opencover -oldstyle \
    -target:"C:\Program Files\dotnet\dotnet.exe" \
    -filter:"+[$projectName]* -[*.Tests]* -[*]*.Api.* -[*]*.Web.* -[xunit.*]*" \
    -hideskipped:All \
    -mergebyhash \
    -excludebyattribute:"*.ExcludeFromCodeCoverage*" \
    -targetargs:"test" \
    -returntargetcode \
    -skipautoprops \
    -mergeoutput -output:"..\\..\\${outputpath}\coverage.xml" -register:user)
done

#---------------------------------#
#      run report generator       #
#---------------------------------#

$reportGenerator -reports:"${outputpath}\coverage.xml" -targetdir:"${outputpath}"

#---------------------------------#
#      show coverage result       #
#---------------------------------#

start chrome "${outputpath}\index.htm"
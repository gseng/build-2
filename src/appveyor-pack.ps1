# pack.ps1

# Get args
$buildConfig=$args[0];

$artifactsDirname="../../artifacts";

if(Test-Path $artifactsDirname) { Remove-Item $artifactsDirname -Force -Recurse }

# Version
$revision = @{ $true = $env:APPVEYOR_BUILD_VERSION ; $false = "6.6.6" }[$env:APPVEYOR_BUILD_VERSION  -ne $NULL];

# Pack it.
foreach ($path in Get-ChildItem ./src/*/*.csproj)
{
    $dirname=$(get-item $path).Directory.FullName;
    & dotnet pack /p:Version=$revision $dirname -c $buildConfig -o $artifactsDirname --no-build
}
#!/bin/sh
set -e

if  [ -n "$ASPNETCORE_ENVIRONMENT" ] && [ "$ASPNETCORE_ENVIRONMENT" != "Development" ]; then envtpl -in /app/appsettings.json.tmpl > /app/appsettings.$ASPNETCORE_ENVIRONMENT.json; fi
dotnet %EXECUTABLE_NAME%.dll
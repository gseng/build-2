#!/bin/bash
set -e

# variables
configuration=${1:-Debug}

# restore
dotnet restore ./%PROJECT_NAME%.sln

# build
dotnet build ./%PROJECT_NAME%.sln
## Description
It's a complete solution for dotnet application that builds on docker 

## How to use
Add the following file to your project's root directory.
Add the following code to the root directory of your project with the name prepare-build.sh. Then replace the specified locations with yours

    #!/bin/bash
    set -e
    # get project name.
    TOKENS=( "PROJECT_NAME:<your-project-name>"
             "PROJECT_NAME_LOWER:<your-project-name-lower-case>"
             "EXECUTABLE_NAME:<your-executable-project-name>" )
    
    # get build files if they are not exist.
    if [ ! -d ./.build/src ]; then
        echo "Build files not found. Downloading from remote repository..."
        git clone https://bitbucket.org/ciceksepeti/build.git .build
    fi
    
    # replace projectname variables to given project name.
    echo "Build files are getting ready..."
    for path in ./.build/src/*; do
        for token in "${TOKENS[@]}" ; do
            KEY="${token%%:*}"
            VALUE="${token##*:}"
            if [[ $(uname) == 'Darwin' ]]; then
                sed -i '' -e "s/%$KEY%/$VALUE/g" $path
            else
                sed -i "s/%$KEY%/$VALUE/g" $path
            fi    
        done
        echo "${path} ok."
    done
    echo "${name} project is ready to build."

After replace the tokens run the following command.

    sh ./prepare-build.sh

Your build files are ready.

Let's try build.sh
    sh .build/src/build.sh

## What this scripts do
- build.sh build project with specified sln file
- test.sh run dotnet test command for per projects that under test folder
- ci-build.sh build your project in a dotnet sdk included docker container then copy your publish dlls to another docker image that dotnet runtime included.
- ci-run.sh run this command after ci-build.sh. It runs the docker container that built via ci-build.sh 
- codecoverage.sh It works only windows based hosts. It calculate codecoverage and publish it to the codecov.io. You must set CODECOV_TOKEN env with your codecove token.
- ci-before-build.sh It pulls dotnet base docker images and puts them a folder that is name  ~/docker. So you can cache that folder for fast builds
- pack.sh It runs dotnet pack command per project that under src folder.